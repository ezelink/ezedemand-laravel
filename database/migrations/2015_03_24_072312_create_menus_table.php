<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parentid')->nullable()->default(0);
			$table->string('text')->nullable();
			$table->string('title')->nullable();
			$table->string('href')->nullable();
			$table->string('icon')->nullable();
			$table->integer('display')->nullable()->default(0);
			$table->integer('access')->nullable()->default(0);
			$table->integer('listorder')->nullable()->default(0);
			$table->integer('modified_by')->nullable()->default(0);
			$table->timestamps('last_modified')->default('CURRENT_TIMESTAMP');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus');
	}

}
