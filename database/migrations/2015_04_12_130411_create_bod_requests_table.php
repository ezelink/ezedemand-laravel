<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBodRequestsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bod_request', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable()->default(0);
			$table->integer('line_id')->nullable()->default(0);
			$table->integer('admin_id')->nullable()->default(0);
			$table->string('username')->nullable();

			$table->integer('bandwidth_up')->nullable()->default(0);
			$table->integer('bandwidth_down')->nullable()->default(0);
			$table->integer('version')->nullable()->default(0);

			$table->string('device_mac')->nullable();
			$table->string('device_name')->nullable();
			$table->dateTime('date_start')->default('CURRENT_TIMESTAMP');
			$table->dateTime('date_end')->default('CURRENT_TIMESTAMP');
			$table->dateTime('date_created')->default('CURRENT_TIMESTAMP');


			$table->integer('unit_price')->nullable()->default(0);
			$table->integer('quantity')->nullable()->default(0);
			$table->string('email')->nullable();
			$table->integer('status')->nullable()->default(0);
			$table->integer('burst')->nullable()->default(0);
			$table->integer('sendmail')->nullable()->default(0);


			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bod_request');
	}

}
