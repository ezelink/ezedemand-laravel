<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminModulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_modules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('userid')->nullable()->default(0);
			$table->integer('moduleid')->nullable()->default(0);
			$table->integer('access')->nullable()->default(0);
			$table->timestamps()->default('CURRENT_TIMESTAMP');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_modules');
	}

}
