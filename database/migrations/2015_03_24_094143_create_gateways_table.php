<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGatewaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lwalias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('mac')->nullable();
			$table->string('vpnip')->nullable();
			$table->string('alias')->nullable();
			$table->integer('groupid')->nullable();
			$table->integer('cityid')->nullable();
			$table->string('building')->nullable();
			$table->string('street',100)->nullable();
			$table->float('latitude')->nullable()->default(null);
			$table->float('longitude')->nullable()->default(null);
			$table->timestamps('lastseen')->default('CURRENT_TIMESTAMP');
			$table->integer('status')->nullable()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lwalias');
	}

}
