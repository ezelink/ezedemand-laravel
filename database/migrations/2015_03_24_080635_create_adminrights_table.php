<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminrightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adminrights', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('userid')->nullable()->default(0);
			$table->integer('moduleid')->nullable()->default(0);
			$table->integer('access')->nullable()->default(0);
			$table->timestamps('lastseen')->default('CURRENT_TIMESTAMP');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adminrights');
	}

}
