@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Bandwidth
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Manage Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/staticreq')}}">Static Bandwidth</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Static Bandwidth Request List</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    @if ( Session::has('flash_message') )
                        <div class="alert alert-danger  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_message') }}
                        </div>
                    @endif
                    @if ( Session::has('flash_success') )
                        <div class="alert alert-success  {{ Session::get('flash_type') }}">
                            <button class="close" data-dismiss="alert"></button>
                            {{ Session::get('flash_success') }}
                        </div>
                    @endif
                    <div class="table-toolbar">
                        <div class="btn-group">
                            <a href="{{URL::to('/newstaticreq')}}">
                                <button class="btn green" >
                                    New Request <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                        <div class="btn-group pull-right">
                            <button data-toggle="dropdown" class="btn dropdown-toggle">Filter List<i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{URL::to('/staticreq/')}}"><?php if($url_id == ""){ echo '<span class="fa fa-check"></span>'; }  ?>All</a></li>
                                <li><a href="{{URL::to('/staticreq/-2')}}"><?php if($url_id == -2){ echo '<span class="fa fa-check"></span>'; }  ?>Booked</a></li>
                                <li><a href="{{URL::to('/staticreq/1')}}"><?php if($url_id == 1){ echo '<span class="fa fa-check"></span>'; }  ?>Active</a></li>
                                <li><a href="{{URL::to('/staticreq/-1')}}"><?php if($url_id == -1){ echo '<span class="fa fa-check"></span>'; }  ?>Completed</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>Client Name</th>
                                <th>Line Interface</th>
                                <th>Bandwidth Down</th>
                                <th>Bandwidth Up</th>
                                <th>Status</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $dt = new DateTime();
                                $current_dt = $dt->format('Y-m-d h:i:s');
                            ?>
                            @foreach($request_list  as $request)

                                <tr>
                                    <td class="highlight">{{$request->alias }}</td>
                                    <td class="hidden-xs">{{$request->interface }}</td>
                                    <td>{{$request->down_name }}</td>
                                    <td>{{$request->up_name }}</td>
                                    <td>
                                        {{--@if($request->date_start <= $current_dt && $request->date_end >= $current_dt && $request->status == 1)
                                            <span class='badge badge-success'>Active</span>
                                        @elseif($request->date_start<= $current_dt && $request->date_end  >= $current_dt && $request->status == 0)
                                            <span class='badge badge-success'>Processing</span>--}}
                                        @if($request->date_start <= $current_dt && $request->date_end >= $current_dt && $request->status == 1)
                                            <span class='badge badge-success'>Active</span>
                                        @elseif($request->date_end  > $current_dt && $request->status == 0)
                                            <span class='badge badge-danger'>Booked</span>
                                        @elseif($request->date_end < $current_dt )
                                            <span class='badge badge-warning'>Completed</span>
                                        @else
                                            <span class='badge badge-warning'>   </span>
                                        @endif
                                    </td>
                                    <td>{{$request->date_start }}</td>
                                    <td>{{$request->date_end }}</td>
                                    <td>
                                        @if($request->date_end < $current_dt)
                                            <i class="fa fa-edit"></i> Edit
                                        @else
                                            <a class="btn default btn-xs purple" href="{{URL::to('/newstaticreq/'.$request->id)}}"><i class="fa fa-edit"></i> Edit</a>
                                        @endif

                                        @if($request->date_end < $current_dt || ($request->date_start <= $current_dt && $current_dt <= $request->date_end) )
                                                <i class="fa fa-trash-o"></i> Delete
                                        @else
                                                <a class="btn default btn-xs black" href="{{URL::to('/destroy_staticreq/'.$request->id)}}"><i class="fa fa-trash-o"></i> Delete</a>
                                        @endif


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection