@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Bandwidth
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Manage Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/staticreq')}}">Static Bandwidth</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/newstaticreq')}}">New Static Request</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>Request New Static Bandwidth
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'storestaticreq','role'=>'form', 'class'=>'form-horizontal')) !!}

                    <div class="form-body">
                        @if(Auth::user()->access == 2 )
                            <div class="form-group">
                                {!! Form::label('groupid','Group',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4">
                                    {!! Form::select('groupid',$group_array,'enabled',array('id'=>'groupid','class'=>'form-control')) !!}
                                    @if ($errors->has('groupid'))
                                        <span class="alert-danger">{{ $errors->first('groupid') }}</span><br>
                                    @endif
                                    @if ($errors->has('user_id'))
                                        <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                    @endif
                                    @if ($errors->has('line_id'))
                                        <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group" id="gateway_area" style="display: none">
                                {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4" id="gateway_result">
                                    {!! Form::select('user_id', $gateway_array, null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        @else
                            <div class="form-group" >
                                {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                                <div class="col-md-4" id="gateway_result">
                                    {!! Form::select('user_id', $gateway_array, null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                                    @if ($errors->has('user_id'))
                                        <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                                    @endif
                                    @if ($errors->has('line_id'))
                                        <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                                    @endif
                                </div>
                            </div>

                        @endif

                        <div class="form-group"  id="line_area" style="display: none">
                            {!! Form::label('line_id','Line',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4"  id="line_result">
                                {!! Form::select('line_id', array(''=>'[Select Gateway First]'), null, ['id' => 'line_id', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('bandwidth_down','Download Bandwidth',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div id="request_bandwidth_down">
                                    {!! Form::select('bandwidth_down',$bandwidth_array,'enabled',array('id'=>'bandwidth_down','class'=>'form-control')) !!}
                                </div>
                                @if ($errors->has('bandwidth_down'))
                                    <span class="alert-danger">{{ $errors->first('bandwidth_down') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('bandwidth_up','Upload Bandwidth',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div id="request_bandwidth_up">
                                    {!! Form::select('bandwidth_up',$bandwidth_array,'enabled',array('id'=>'bandwidth_up','class'=>'form-control')) !!}
                                </div>
                                @if ($errors->has('bandwidth_up'))
                                    <span class="alert-danger">{{ $errors->first('bandwidth_up') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_range','Inclusive Date',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="2012-11-12" data-date-format="yyyy-mm-dd">
                                    {!! Form::text('date_start','',array('id'=>'date_start','class'=>'form-control')) !!}
                                    <span class="input-group-addon">to</span>
                                    {!! Form::text('date_end','',array('id'=>'date_end','class'=>'form-control')) !!}

                                </div>
                                <!-- /input-group -->
                                <span class="help-block">Select date range</span>
                                @if ($errors->has('date_start'))
                                    <span class="alert-danger">{{ $errors->first('date_start') }}</span>
                                @endif
                                @if ($errors->has('date_end'))
                                    <span class="alert-danger">{{ $errors->first('date_end') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Save',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/staticreq')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection