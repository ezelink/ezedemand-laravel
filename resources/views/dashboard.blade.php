@extends('app')

@section('content')
	<!-- BEGIN PAGE HEADER-->
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title">
				Dashboard
			</h3>
			<ul class="page-breadcrumb breadcrumb">
				<li>
					<i class="fa fa-home"></i>
					<a href="{{URL::to('/dashboard')}}">Dashboard</a>
				</li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat blue">
				<div class="visual">
					<i class="fa fa-cloud"></i>
				</div>
				<div class="details">
					<div class="number">
						{{ $active_gateway }}
					</div>
					<div class="desc">
						Online Gateways
					</div>
				</div>
				<a href="{{URL::to('/gateway/1')}}" class="more">
					View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat red">
				<div class="visual">
					<i class="fa fa-user-md"></i>
				</div>
				<div class="details">
					<div class="number">{{ $offline_gateway }}</div>
					<div class="desc">Gateways Down</div>
				</div>
				<a href="{{URL::to('/gateway/0')}}" class="more">
					View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat blue">
				<div class="visual">
					<i class="fa fa-bullhorn"></i>
				</div>
				<div class="details">
					<div class="number">{{ $pending_request }}</div>
					<div class="desc">Pending Requests</div>
				</div>
				<a href="{{URL::to('/staticreq/-2')}}" class="more">
					View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="dashboard-stat yellow">
				<div class="visual">
					<i class="fa fa-check"></i>
				</div>
				<div class="details">
					<div class="number">{{ $active_request }}</div>
					<div class="desc">Active Requests</div>
				</div>
				<a href="{{URL::to('/staticreq/1')}}" class="more">
					View more <i class="m-icon-swapright m-icon-white"></i>
				</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 ">
			<!-- BEGIN PORTLET-->
			<div class="portlet box blue">
				<div class="portlet-title">
					<div class="caption"><i class="fa fa-bar-chart-o"></i>System Log</div>
					<div class="tools">
						<a href="javascript:;" class="collapse"></a>
						<a href="javascript:;" class="reload"></a>

					</div>
				</div>
				<div class="portlet-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>#</th>
								<th>Administrator</th>
								<th>Action</th>
							</tr>
							</thead>
							<tbody>
							<?php  $no =1; ?>
							@foreach($log_list  as $log)
								<tr>
									<td class="highlight">{{ $no }}</td>
									<td class="hidden-xs">{{ $log->username }}</td>
									<td>{{ $log->created_at.' - '.$log->action }}</td>
								</tr>
								<?php  $no ++; ?>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- END PORTLET-->
		</div>

	</div>
@endsection