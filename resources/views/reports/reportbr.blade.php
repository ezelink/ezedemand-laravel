@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Reports
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Reports</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/reportbr')}}">Bandwidth Request</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Bandwidth Request Reports</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body " style="display: block;">
                    {!! Form::open(array('url'=>'generate_reportbr','role'=>'form', 'class'=>'form-horizontal')) !!}
                    <div class="col-md-12 form-group">
                        {!! Form::label('brreport_types','Select Report Type',array('class'=>' control-label')) !!}
                        {!! Form::select('report_types', array(''=>'[Select a Report Type]','usage' =>'Usage Reports', 'revenue'=>'Revenue Reports', 'subscriber'=>'Subscriber Reports'), 'default' , ['id' => 'br_report_types', 'class' => 'form-control']) !!}
                        @if ($errors->has('report_types'))
                            <span class="alert-danger">{{ $errors->first('report_types') }}</span><br>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('groupid','Group',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-4">
                            {!! Form::select('groupid',$group_array,'enabled',array('id'=>'groupid','class'=>'form-control')) !!}
                            @if ($errors->has('groupid'))
                                <span class="alert-danger">{{ $errors->first('groupid') }}</span><br>
                            @endif
                            @if ($errors->has('user_id'))
                                <span class="alert-danger">{{ $errors->first('user_id') }}</span><br>
                            @endif
                            @if ($errors->has('line_id'))
                                <span class="alert-danger">{{ $errors->first('line_id') }}</span><br>
                            @endif
                        </div>
                    </div>
                    <div class="form-group" id="gateway_area" style="display: none">
                        {!! Form::label('user_id','Gateway',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-4" id="gateway_result">
                            {!! Form::select('user_id', array(''=>'[Pls.Select Group First]'), null, ['id' => 'user_id', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group"  id="line_area" style="display: none">
                        {!! Form::label('line_id','Line',array('class'=>'col-md-3 control-label')) !!}
                        <div class="col-md-4"  id="line_result">
                            {!! Form::select('line_id', array(''=>'[Pls.Select Group & Gateway First]'), null, ['id' => 'line_id', 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-3" ></div>
                        <div class="col-md-4" >
                            @if ($errors->has('date_start'))
                                <span class="alert-danger">{{ $errors->first('date_start') }}</span><br>
                            @endif
                            @if ($errors->has('date_end'))
                                <span class="alert-danger">{{ $errors->first('date_end') }}</span><br>
                            @endif
                            @if ($errors->has('month'))
                                <span class="alert-danger">{{ $errors->first('month') }}</span><br>
                            @endif

                        </div>

                    </div>

                    <div class="form-group"  id="reportbr_period" >
                        <div id="month_select" style="display: none">
                            <label class="control-label col-md-3" for="inputSuccess">Month</label>
                            <div  class="col-md-4" >
                                <select name="month" class="form-control" id="month">
                                    <option value=''>Select a Month</option>
                                    <?php
                                    for ($x = 1; $x <= 12; $x++) {
                                        $value = date("Y-m",strtotime("-".$x." Months"));
                                        $data = date("Y-F",strtotime("-".$x." Months"));
                                        echo "<option value='".$value."'>".$data."</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                        </div>
                        <div id="date_select" style="display: none">
                            {!! Form::label('date_range','Date Range',array('class'=>'control-label col-md-3')) !!}
                            <div class="col-md-4">
                                <div class="input-group input-large date-picker input-daterange" data-date="2012-11-12" data-date-format="yyyy-mm-dd">
                                    {!! Form::text('date_start','',array('id'=>'filter_date_start','class'=>'form-control')) !!}
                                    <span class="input-group-addon">to</span>
                                    {!! Form::text('date_end','',array('id'=>'filter_date_end','class'=>'form-control')) !!}

                                </div>
                                <!-- /input-group -->
                                <span class="help-block">Select date range</span>
                                @if ($errors->has('date_start'))
                                    <span class="alert-danger">{{ $errors->first('date_start') }}</span>
                                @endif
                                @if ($errors->has('date_end'))
                                    <span class="alert-danger">{{ $errors->first('date_end') }}</span>
                                @endif
                            </div>

                        </div>

                    </div>

                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Generate Report',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/reportbr')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>

                    <div class="form-group"  id="admin_rights_result">

                    </div>
                    {!! Form::close() !!}

                </div>
            </div>

        </div>

    </div>
@endsection