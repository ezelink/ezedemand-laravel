<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eZeDemand | eZeLink Telecom</title>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ asset('/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="{{ asset('/assets/css/style-metronic.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/css/style-responsive.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="{{ asset('/assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/assets/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="{{ asset('/assets/css/pages/login.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->

    @yield('headstyle')

</head>
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <img src="../images/ezedemand_logo.png" alt="" />
</div>
<!-- END LOGO -->
<!-- BEGIN CONTAINER -->
<div >
    <!-- BEGIN PAGE -->
    {{--<div class="page-content">--}}
        @yield('content')
    {{--</div>--}}
    <!-- END PAGE -->
</div>
<!-- END CONTAINER -->
</body>
</html>
