@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Gateway
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Manage Gateway</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/gateway')}}">Gateways</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/edit_gateway/'.$gateway_data->id)}}">Update Gateway</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Update a Gateway
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'update_gateway','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $gateway_data->id, array('id' => 'invisible_id')) !!}

                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::label('name','Name for Gateway',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('name',$gateway_data->alias,array('id'=>'name','class'=>'form-control','placeholder'=>'Type a Name')) !!}
                                    @if ($errors->has('name'))
                                        <span class="alert-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('mac','Mac Address',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('mac_disable',$gateway_data->mac,array('id'=>'mac','class'=>'form-control','disabled'=>'disabled')) !!}
                                    {!! Form::hidden('mac', $gateway_data->mac, array('id' => 'invisible_mac')) !!}
                                    @if ($errors->has('mac'))
                                        <span class="alert-danger">{{ $errors->first('mac') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('groupid','Group',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                {!! Form::select('groupid',$group_array,$gateway_data->groupid,array('id'=>'groupid','class'=>'form-control')) !!}
                                @if ($errors->has('groupid'))
                                    <span class="alert-danger">{{ $errors->first('groupid') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('building','Building No/Name',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('building',$gateway_data->building,array('id'=>'building','class'=>'form-control','placeholder'=>'Type a Building No/Name')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('street','Street',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('street',$gateway_data->street,array('id'=>'street','class'=>'form-control','placeholder'=>'Type a Street')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('city','City',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4" id="city_result">
                                {!! Form::select('city', $city_array, $gateway_data->cityid, ['id' => 'city', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('country','Country',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-4">
                                {!! Form::select('country', $country_array, $gateway_data->country, ['id' => 'country', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('latitude','Latitude',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('latitude',$gateway_data->latitude,array('id'=>'latitude','class'=>'form-control','placeholder'=>'Type a latitude')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('longitude','Longitude',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                <div class="input-group">
                                    {!! Form::text('longitude',$gateway_data->longitude,array('id'=>'longitude','class'=>'form-control','placeholder'=>'Type a longitude')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <thead>
                                    <th>Line</th>
                                    <th>Client Mac</th>
                                    <th>Account No</th>
                                    <th>Default Down</th>
                                    <th>Max Down</th>
                                    <th>Default Up</th>
                                    <th>Max Up</th>
                                    <th>Status</th>
                                    </thead>
                                    <tbody>
                                    @if(!$line_list)
                                        <tr>
                                            <td colspan="8">No Lines Setup for this Gateway</td>
                                        </tr>


                                    @else
                                        @foreach($line_list  as $lines)
                                            <tr rowid="{{$lines->id }}">
                                                <td>{{$lines->interface }}</td>
                                                <td>{{$lines->client_mac }}</td>
                                                <td>
                                                    {!! Form::text('line_data['.$lines->id.'][account_no]',$lines->account_no ,array('class'=>'form-control','placeholder'=>'Type Account Number')) !!}
                                                </td>
                                                <td>
                                                    {!! Form::select('line_data['.$lines->id.'][down_default]',$bandwidth_array,$lines->down_default,array('class'=>'line_default_down form-control')) !!}
                                                </td>
                                                <td id="line_max_down_{{$lines->id }}">{!! ($lines->down_max)? $lines->down_max.'Mbps':'null'   !!}</td>
                                                <td>
                                                    {!! Form::select('line_data['.$lines->id.'][up_default]',$bandwidth_array,$lines->up_default,array('class'=>'line_default_up  form-control')) !!}
                                                </td>
                                                <td id="line_max_up_{{$lines->id }}">{!! ($lines->up_max)? $lines->up_max.'Mbps':'null'   !!}</td>
                                                <td>
                                                    {!! Form::checkbox('line_data['.$lines->id.'][status]', $lines->status, $lines->status, ['class' => 'form-control']) !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif


                                    </tbody>


                                </table>

                            </div>
                        </div>


                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Update',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/gateway')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection