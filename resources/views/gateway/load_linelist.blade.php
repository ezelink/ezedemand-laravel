<table class="table table-bordered">
    <thead>
    <th>Line</th>
    <th>Client Mac</th>
    <th>Account No</th>
    <th>Default Down</th>
    <th>Max Down</th>
    <th>Default Up</th>
    <th>Max Up</th>
    <th>Status</th>
    </thead>
    <tbody>
    @if(!$line_list)
        <tr>
            <td colspan="8">No Lines Setup for this Gateway</td>
        </tr>


    @else
        @foreach($line_list  as $lines)
            <tr>
                <td>{{$lines->interface }}</td>
                <td>{{$lines->client_mac }}</td>
                <td>{{$lines->account_no }}</td>
                <td>{{$lines->down_name }}</td>
                <td>{{$lines->down_max }} mbps</td>
                <td>{{$lines->up_name }}</td>
                <td>{{$lines->up_max }} mbps</td>
                <td>{!! ($lines->status == 1)? "<span class='badge badge-success'>Active</span>" : "<span class='badge badge-warning'>Offline</span>"  !!}</td>
            </tr>
        @endforeach
    @endif


    </tbody>


</table>