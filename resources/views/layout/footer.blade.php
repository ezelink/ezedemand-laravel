<div class="footer">
    <div class="footer-inner">
        2015 &copy; EmiratesWiFi. Powered by eZeDemand
    </div>
    <div class="footer-tools">
			<span class="go-top">
			<i class="fa fa-angle-up"></i>
			</span>
    </div>
</div>

<!-- Scripts -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="{{ asset('/assets/plugins/respond.min.js') }}"></script>
<script src="{{ asset('/assets/plugins/respond.min.js') }}assets/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="{{ asset('/assets/plugins/jquery-1.10.2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/jquery-migrate-1.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/jquery.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/plugins/uniform/jquery.uniform.min.js') }}" type="text/javascript" ></script>

<script type="text/javascript" src="{{ asset('/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/plugins/bootstrap-daterangepicker/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
<!-- END CORE PLUGINS -->
<script src="{{ asset('/assets/scripts/app.js') }}"></script>
<script src="{{ asset('/assets/scripts/form-components.js') }}"></script>

<script src="{{ asset('/assets/scripts/custom.js') }}"></script>
<!-- END JAVASCRIPTS -->