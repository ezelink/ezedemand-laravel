<!-- BEGIN SIDEBAR -->
<div class="page-sidebar navbar-collapse collapse" style="position:fixed">

    <ul class="page-sidebar-menu">
        <li>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <div class="sidebar-toggler hidden-phone"></div>
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li  {{ Request::is('dashboard') ? ' class=start active' : ' class=start'}}>
            <a href="{{URL::to('/dashboard')}}">
                <i class="fa fa-home"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li></li>

        <?php
            if(Auth::user()->access == 2){
                $menulist = DB::table('menus')->where('display', 1 )->orderBy('listorder', 'asc')->get();
            }elseif(Auth::user()->access == 1){
                $menulist = DB::table('menus')->where('display', 1 )->where('access', '!=', 2 )->orderBy('listorder', 'asc')->get();
            }elseif(Auth::user()->access == 0){
                $menulist = DB::table('menus')->where('display', 1 )->where('access', '=', 0 )->orderBy('listorder', 'asc')->get();
            }

        $right_list = DB::table('admin_modules')->select('moduleid')->where('userid', Auth::user()->id)->get();
        $allowed_rights = array();
        foreach ($right_list as $right){
            $allowed_rights[] = $right->moduleid;

        }
        $current_href = Request::segment(1);

        if($current_href){

        $active_parent = DB::table('menus')
                ->select('parentid')
                ->where('href', $current_href )
                ->first();

        $active_super_parent = DB::table('menus as menu')
                ->join('menus as mmenu', 'menu.id', '=', 'mmenu.parentid')
                ->select('menu.parentid')
                ->where('mmenu.href', $current_href )
                ->first();
            if($active_super_parent){
                $active_super_parentid = $active_super_parent->parentid;
            }else{
                $active_super_parentid = 0;
            }
        $active_parentid = $active_parent->parentid;

        }else{
            $active_super_parentid = 0;
            $active_parentid = 0;


        }




        $parent_list = array();

        ?>








        @foreach ($menulist as $menu)
            <?php
            $parent_list[] = $menu->parentid;
                if($current_href && $current_href != 'dashboard'){
                    $mainclass = ($active_parent->parentid == $menu->id || $active_super_parentid == $menu->id )? ' class=active' : '';
                }else{
                    $mainclass = "";
                }

            ?>


            @if($menu->parentid == 0)
                @if((Auth::user()->access == 2) || (Auth::user()->access != 2 && $allowed_rights))
                    <li  {{ $mainclass  }} ><a href='' >
                        <i class='{{ $menu->icon }}'></i>
                        <span class='title'>{{ $menu->text }}</span>
                        <span class='arrow '></span>
                    </a>

                        <ul class='sub-menu'>
                @endif
                        @foreach ($menulist as $submenu)

                                <?php
                                $subclass = ($active_parent->parentid == $submenu->id || Request::is($submenu->href) )? ' class=active' : '';

                                ?>

                         @if(Auth::user()->access == 2)
                            @if($submenu->parentid == $menu->id)
                                <li  {{ $subclass }} >
                                <a href='{{ URL::to($submenu->href)  }}' >
                                    <i class='{{ $submenu->icon }}'></i>{{ $submenu->text }}
                                </a>

                                @if (in_array($menu->id, $parent_list ))
                                    <ul class='sub-menu'>
                                    @foreach ($menulist as $childmenu)
                                        {{ $childclass = Request::is($childmenu->href)? ' class=active' : ''   }}
                                        @if($childmenu->parentid == $submenu->id){
                                            <li {{ $childclass }} >
                                            <a href='{{ URL::to($childmenu->href) }}' >
                                            <i class='{{ $childmenu->icon }}'></i>
                                                {{ $childmenu->text }}
                                            </a>
                                            </li>
                                        @endif
                                    @endforeach
                                    </ul>

                                @endif

                                </li>
                            @endif
                        @endif
                        @if(Auth::user()->access != 2 && $allowed_rights)
                            @if($submenu->parentid == $menu->id  && in_array($submenu->id, $allowed_rights ) )
                                <li  {{ $subclass }} >
                                    <a href='{{ URL::to($submenu->href)  }}' >
                                        <i class='{{ $submenu->icon }}'></i>{{ $submenu->text }}
                                    </a>

                                    @if (in_array($menu->id, $parent_list ))
                                        <ul class='sub-menu'>
                                            @foreach ($menulist as $childmenu)
                                                {{ $childclass = Request::is($childmenu->href)? ' class=active' : ''   }}
                                                @if($childmenu->parentid == $submenu->id){
                                                <li {{ $childclass }} >
                                                    <a href='{{ URL::to($childmenu->href) }}' >
                                                        <i class='{{ $childmenu->icon }}'></i>
                                                        {{ $childmenu->text }}
                                                    </a>
                                                </li>
                                                @endif
                                            @endforeach
                                        </ul>

                                    @endif

                                </li>
                            @endif


                        @endif
                        @endforeach
                @if((Auth::user()->access == 2) || (Auth::user()->access != 2 && $allowed_rights))
                </ul>
                </li>
                @endif
            @endif
        @endforeach


    </ul>
</div>
<!-- END SIDEBAR -->