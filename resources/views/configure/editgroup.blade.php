@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/groups')}}">Manage Groups</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/newgroup/'.$group->id)}}">Update Group</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Update a Group
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'update_group','role'=>'form', 'class'=>'form-horizontal')) !!}

                    {!! Form::hidden('id', $group->id, array('id' => 'invisible_id')) !!}

                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::label('name','Group Name',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('name',($group->name)? $group->name : '',array('id'=>'','class'=>'form-control','placeholder'=>'Enter a Group Name')) !!}
                                @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('description','Description',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('description',($group->description)? $group->description : '',array('id'=>'description','class'=>'form-control','placeholder'=>'Type small description')) !!}
                                @if ($errors->has('description'))
                                    <span class="alert-danger">{{ $errors->first('description') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Update',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/groups')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection