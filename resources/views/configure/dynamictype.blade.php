@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/dynamictype')}}">Dynamic Bandwidth Types</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-picture"></i>Dynamic Bandwidth Type list</div>
                    <div class="tools">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="reload" href="javascript:;"></a>
                    </div>
                </div>

                <div class="portlet-body" style="display: block;">
                    <div class="table-toolbar">
                        <div class="btn-group">
                            <a href="{{URL::to('/newdynamic')}}">
                                <button class="btn green" >
                                    Add New <i class="fa fa-plus"></i>
                                </button>
                            </a>
                        </div>
                        <div class="btn-group pull-right">
                            <button data-toggle="dropdown" class="btn dropdown-toggle">Tools <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">Print</a></li>
                                <li><a href="#">Save as PDF</a></li>
                                <li><a href="#">Export to Excel</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Min Value(Mbps)</th>
                                <th>Max Value(Mbps)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bandwidth_list  as $bandwidth)
                                <tr>
                                    <td class="highlight">{{$bandwidth->bod_name }}</td>
                                    <td class="hidden-xs">{{$bandwidth->price }}</td>
                                    <td>{{$bandwidth->value }}</td>
                                    <td>{{$bandwidth->max_value }}</td>
                                    <td>
                                        <a class="btn default btn-xs purple" href="{{URL::to('/newdynamic/'.$bandwidth->id)}}"><i class="fa fa-edit"></i> Edit</a>
                                        <a class="btn default btn-xs black" href="{{URL::to('/destroy_dynamic/'.$bandwidth->id)}}"><i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection