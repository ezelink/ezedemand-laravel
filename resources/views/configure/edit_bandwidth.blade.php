@extends('app')

@section('content')
    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Configuration
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="fa fa-cogs"></i>
                    <a href="#">Configuration</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="{{URL::to('/default_bandwidth')}}">Default Bandwidth Value</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li><a href="{{URL::to('/new_bandwidth/'.$bandwidth->id)}}">Update a Default Bandwidth</a></li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i> Update Default Bandwidth Value
                    </div>
                    <div class="tools">
                        <a class="collapse" href=""></a>
                        <a class="reload" href=""></a>
                    </div>
                </div>
                <div class="portlet-body form">

                    {!! Form::open(array('url'=>'update_bandwidth','role'=>'form', 'class'=>'form-horizontal')) !!}
                    {!! Form::hidden('id', $bandwidth->id, array('id' => 'invisible_id')) !!}

                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::label('name','Bandwidth Name',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('name',$bandwidth->name,array('id'=>'','class'=>'form-control','placeholder'=>'Enter a Bandwidth  Name')) !!}
                                @if ($errors->has('name'))
                                    <span class="alert-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('value','Bandwidth Value(Mbps)',array('class'=>'col-md-3 control-label')) !!}
                            <div class="col-md-9">
                                {!! Form::text('value',$bandwidth->value,array('id'=>'value','class'=>'form-control','placeholder'=>'Type Bandwidth Value')) !!}
                                @if ($errors->has('value'))
                                    <span class="alert-danger">{{ $errors->first('value') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            {!! Form::submit('Update',array('class'=>'btn green')) !!}
                            <a href="{{URL::to('/default_bandwidth')}}"><button class="btn default" type="button">Cancel</button></a>
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>
        </div>

    </div>
@endsection