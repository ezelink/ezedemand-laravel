<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>eZeDemand | eZeLink Telecom</title>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="{{ asset('/assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/assets/plugins/uniform/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN THEME STYLES -->
	<link href="{{ asset('/assets/css/style-metronic.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/assets/css/style-responsive.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/assets/css/plugins.css') }}" rel="stylesheet" type="text/css"/>
	<link href="{{ asset('/assets/css/themes/default.css') }}" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="{{ asset('/assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>
	<!-- END THEME STYLES -->

	<!-- BEGIN MODULE STYLES -->
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/plugins/bootstrap-datepicker/css/datepicker.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/plugins/bootstrap-timepicker/compiled/timepicker.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('/assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css') }}" />
	<!-- END MODULE STYLES -->

	@yield('headstyle')

</head>
<body class="page-header-fixed">
<!-- BEGIN HEADER -->
@include('layout.headerbar')
<!-- END HEADER -->
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	@include('layout.sidebar-old')

	<!-- END SIDEBAR -->
	<!-- BEGIN PAGE -->
	<div class="page-content">
		@yield('content')
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN  FOOTER -->
<footer> @include('layout.footer') </footer>
<!-- END FOOTER -->
</body>
</html>
