<?php namespace App\Http\Controllers;

use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DashboardController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct()
	{
		$this->middleware('auth');
	}


	public function index()
	{

		if(Auth::user()->access == 2 ){
			$log_list = DB::table('action_logs')->orderBy('id', 'desc')->take(10)->get();
			$active_gateway = DB::table('lwalias')->where('status', '=', 1)->count();
			$offline_gateway = DB::table('lwalias')->where('status', '=', 0)->count();
			$pending_request = DB::table('bod_request')->where('status', '=', 0)->where('burst', '=', 0)->count();
			$active_request = DB::table('bod_request')->where('status', '=', 1)->where('burst', '=', 0)->count();


		}elseif( Auth::user()->access == 1 ){
			$log_list = DB::table('action_logs')
						->leftJoin('admins', 'admins.id', '=', 'action_logs.userid')
						->where('admins.groupid', '=', Auth::user()->groupid)
						->orderBy('action_logs.id', 'desc')
						->take(10)
						->get();


			$active_gateway = DB::table('lwalias')->where('status', '=', 1)->where('groupid', '=', Auth::user()->groupid)->count();
			$offline_gateway = DB::table('lwalias')->where('status', '=', 0)->where('groupid', '=', Auth::user()->groupid)->count();
			$pending_request = DB::table('bod_request')
								->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
								->where('bod_request.status', '=', 0)
								->where('bod_request.burst', '=', 0)
								->where('lwalias.groupid', '=', Auth::user()->groupid)
								->count();
			$active_request = DB::table('bod_request')
								->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
								->where('bod_request.status', '=', 1)
								->where('bod_request.burst', '=', 0)
								->where('lwalias.groupid', '=', Auth::user()->groupid)
								->count();

		}else{
			$log_list = DB::table('action_logs')->where('userid', '=', Auth::user()->id )->orderBy('id', 'desc')->take(10)->get();
			$active_gateway = DB::table('lwalias')->where('status', '=', 1)->where('groupid', '=', Auth::user()->groupid)->count();
			$offline_gateway = DB::table('lwalias')->where('status', '=', 0)->where('groupid', '=', Auth::user()->groupid)->count();
			$pending_request = DB::table('bod_request')
				->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
				->where('bod_request.status', '=', 0)
				->where('bod_request.burst', '=', 0)
				->where('lwalias.groupid', '=', Auth::user()->groupid)
				->count();
			$active_request = DB::table('bod_request')
				->leftJoin('lwalias', 'lwalias.id', '=', 'bod_request.user_id')
				->where('bod_request.status', '=', 1)
				->where('bod_request.burst', '=', 0)
				->where('lwalias.groupid', '=', Auth::user()->groupid)
				->count();

		}


		return view('dashboard',
			['log_list' => $log_list,
				'active_gateway' => $active_gateway,
				'offline_gateway' => $offline_gateway,
				'pending_request' => $pending_request,
				'active_request' => $active_request]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
