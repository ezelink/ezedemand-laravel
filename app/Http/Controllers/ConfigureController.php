<?php
namespace App\Http\Controllers;

use App\Admins;
use App\Groups;
use App\Bod_bandwidth;
use App\Default_bandwidth;
use App\Admin_modules;
use App\Interfaces;
use App\Action_log;
use DB;
use Session;
Use Validator;
use Auth;
use Hash;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

class ConfigureController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Display the list of Administrator.
	 *
	 * @return Response
	 */
	public function administrator()
	{
		if(Auth::user()->access == 2){
			$admin_list = DB::table('admins')->get();
		}elseif(Auth::user()->access == 1){
			$admin_list = DB::table('admins')->where('groupid', '=', Auth::user()->groupid)->get();

		}else{
			$admin_list = DB::table('admins')->where('id', '=', Auth::user()->id)->get();
		}

		return view('configure.administrator', ['admin_list' => $admin_list]);
	}


	/**
	 * Show  the form for editing the specified/New User/Admin.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function new_admin($id = null )
	{
		$group_list = DB::table('groups')->select('id', 'name')->get();
		$group_array[''] ="[Select a Group]";
		foreach($group_list  as $group){
			$group_array[$group->id]= $group->name;

		}

		if(Auth::user()->access == 2 ){
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->get();
		}else{
			$gateway_list = DB::table('lwalias')->select('id', 'alias')->where('groupid', '=', Auth::user()->groupid )->get();
		}
		$gateway_array[0] ="[Select a Gateway]";
		foreach($gateway_list  as $gateway){
			$gateway_array[$gateway->id]= $gateway->alias;

		}


		if($id){
			$administrator = DB::table('admins')->where('id', '=', $id)->first();
			if(Auth::user()->access == 2){
				return view('configure.edit_admin', ['administrator' => $administrator,'group_array' => $group_array,'gateway_array' => $gateway_array]);
			}else{
				return view('configure.edit_admin', ['group_array' => $group_array, 'administrator' => $administrator,'gateway_array' => $gateway_array]);
			}

		}else{
			if(Auth::user()->access == 2){
				return view('configure.new_admin', ['group_array' => $group_array,'gateway_array' => $gateway_array]);
			}else{
				return view('configure.new_admin', ['group_array' => $group_array,'gateway_array' => $gateway_array]);
			}
		}

	}





	/**
	 * Store a new admin in storage.
	 *
	 * @return administrator-list-view
	 */
	public function regadmin(Request $request)
	{
		if(Auth::user()->access == 2){
			$v = Validator::make($request->all(), [
				'name' => 'required | unique:admins',
				'email' => 'required|email',
				'groupid' => 'required|Integer',
				'access' => 'required|Integer',
				'password' => 'required|min:3|confirmed',
				'password_confirmation' => 'required|min:3',
			]);

		}else{
			$v = Validator::make($request->all(), [
				'name' => 'required | unique:admins',
				'email' => 'required|email',
				'email' => 'required|email',
				'user_id' => 'required|Integer',
				'password' => 'required|min:3|confirmed',
				'password_confirmation' => 'required|min:3',
			]);
		}

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			if($request->input('access') == 1){

				$menulist = DB::table('menus')->where('access', '!=', 2)->where('display', '=', 1)->get();
			}elseif($request->input('access') == 2){

				$menulist = DB::table('menus')->where('display', '=', 1)->get();
			}else{

				$menulist = DB::table('menus')->where('access', '=', 0)->where('display', '=', 1)->get();
			}


			$admin = new Admins();
			/*$admin->account_no = $request->input('account_no');*/
			$admin->email = $request->input('email');
			if(Auth::user()->access == 2){
				$admin->groupid = $request->input('groupid');
			}else{
				$admin->groupid = Auth::user()->groupid;
				$admin->aliasid = $request->input('user_id');
			}
			$admin->name = $request->input('name');
			$admin->password = Hash::make($request->input('password'));
			$admin->contact = $request->input('contact');
			$admin->access = $request->input('access');
			$admin->save();

			$the_id = $admin->id;



			foreach($menulist as $menu ){
				$modules = new Admin_modules();
				$modules->userid = $the_id;
				$modules->moduleid = $menu->id;
				$modules->access = 1;
				$modules->save();
			}

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' New User Regisred Successfully';
			$action_log->save();
			/* action log insertion */


			return redirect('administrator')->with('flash_success', ' New User Regisred Successfully!.');

		}

	}

	/**
	 * Store Updated records of admin/Users in storage.
	 *
	 * @param  array  $request
	 * @return admin/Users list view
	 */
	public function update_admin(Request $request)
	{
		if(Auth::user()->access == 2){
			if(Auth::user()->id == $request->input('id')){
				$v = Validator::make($request->all(), [
					'email' => 'required|email',
					'password' => 'required|min:3|confirmed',
					'password_confirmation' => 'required|min:3',
				]);

			}else{
				$v = Validator::make($request->all(), [
					'email' => 'required|email',
					'groupid' => 'required|Integer',
					'password' => 'required|min:3|confirmed',
					'password_confirmation' => 'required|min:3',
					'access' => 'required|Integer',
				]);
			}

		}elseif(Auth::user()->access == 1){
			if(Auth::user()->id == $request->input('id')){
				$v = Validator::make($request->all(), [
					'email' => 'required|email',
					'password' => 'required|min:3|confirmed',
					'password_confirmation' => 'required|min:3',
				]);

			}else{
				$v = Validator::make($request->all(), [
					'email' => 'required|email',
					'user_id' => 'required|Integer',
					'password' => 'required|min:3|confirmed',
					'password_confirmation' => 'required|min:3',
					'access' => 'required|Integer',
				]);

			}
		}else{
			$v = Validator::make($request->all(), [
				'email' => 'required|email',
				'user_id' => 'required|Integer',
				'password' => 'required|min:3|confirmed',
				'password_confirmation' => 'required|min:3',
				'access' => 'required|Integer',
			]);

		}


		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$administrator = DB::table('admins')->where('id', '=', $request->input('id'))->first();

			if($administrator->access != $request->input('access')){
				if($request->input('access') == 1){

					$menulist = DB::table('menus')->where('access', '!=', 2)->where('display', '=', 1)->get();
				}elseif($request->input('access') == 2){

					$menulist = DB::table('menus')->where('display', '=', 1)->get();
				}else{

					$menulist = DB::table('menus')->where('access', '=', 0)->where('display', '=', 1)->get();
				}


				Admin_modules::where('userid', '=',  $request->input('id'))->delete();

				foreach($menulist as $menu ){
					$modules = new Admin_modules();
					$modules->userid = $request->input('id');
					$modules->moduleid = $menu->id;
					$modules->access = 1;
					$modules->save();
				}


			}

			$admin = Admins::find($request->input('id'));
			/*$admin->account_no = $request->input('account_no');*/
			$admin->email = $request->input('email');
			if(Auth::user()->access == 2){

				if(Auth::user()->id == $request->input('id')){
					$admin->access = 2;
				}else{
					$admin->groupid = $request->input('groupid');
					$admin->aliasid = $request->input('user_id');
					$admin->access = $request->input('access');
				}
			}elseif(Auth::user()->access == 1){
				if(Auth::user()->id == $request->input('id')){
					$admin->access = 1;
				}else{
					$admin->groupid = Auth::user()->groupid;
					$admin->aliasid = $request->input('user_id');
				}

			}else{
				$admin->groupid = Auth::user()->groupid;
				$admin->aliasid = $request->input('user_id');
				$admin->access = $request->input('access');
			}
			$admin->contact = $request->input('contact');
			$admin->password = Hash::make($request->input('password'));
			$admin->save();




			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Registered User Updated Successfully ';
			$action_log->save();
			/* action log insertion */


			return redirect('administrator')->with('flash_success', ' Registered User Updated Successfully!.');

		}

	}


	/**
	 * Remove the specified User/Admin from storage.
	 *
	 * @param  int  $id
	 * @return User/admin listing page.
	 */
	public function destroy_admin($id)
	{
		$affectedRows = Admins::where('id', '=', $id)->delete();

		if($affectedRows){

			Admin_modules::where('userid', '=',  $id)->delete();
			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Registered User Deleted Successfully ';
			$action_log->save();
			/* action log insertion */


			return redirect('administrator')->with('flash_success', ' Registered User Deleted Successfully!.');
		}else{
			return redirect('administrator')->with('flash_message', 'User not deleted,Try again...');
		}

	}



	/**
	 * Display the list of default Bandwidth type.
	 *
	 * @return Response
	 */
	public function default_bandwidth()
	{
		$bandwidth_list = DB::table('default_bandwidth')->get();

		return view('configure.default_bandwidth', ['bandwidth_list' => $bandwidth_list]);
	}


	/**
	 * Show  the form for editing the specified/New Default Bandwidth Type.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function new_bandwidth($id = null )
	{
		if($id){
			$bandwidth = DB::table('default_bandwidth')->where('id', '=', $id)->first();

			return view('configure.edit_bandwidth', ['bandwidth' => $bandwidth]);

		}else{
			return view('configure.new_bandwidth');
		}

	}

	/**
	 * Store a newly created Default Bandwidth type in storage.
	 *
	 * @return Response
	 */
	public function store_bandwidth(Request $request)
	{
		$v = Validator::make($request->all(), [
				'name' => 'required',
				'value' => 'required|Integer',
			]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{


			$admin = new Default_bandwidth();
			/*$admin->account_no = $request->input('account_no');*/
			$admin->name = $request->input('name');
			$admin->value = $request->input('value');
			$admin->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' New Default Bandwidth value created Successfully!';
			$action_log->save();
			/* action log insertion */


			return redirect('default_bandwidth')->with('flash_success', ' New Default Bandwidth value created Successfully!.');

		}

	}

	/**
	 * Store Updated records of Default Bandwidth in storage.
	 *
	 * @param  array  $request
	 * @return Default Bandwidth list view
	 */
	public function update_bandwidth(Request $request)
	{
		$v = Validator::make($request->all(), [
					'name' => 'required',
					'value' => 'required |Integer',
				]);


		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$admin = Default_bandwidth::find($request->input('id'));
			$admin->name = $request->input('name');
			$admin->value = $request->input('value');
			$admin->save();




			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Default Bandwidth value updated Successfully! ';
			$action_log->save();
			/* action log insertion */


			return redirect('default_bandwidth')->with('flash_success', ' Default Bandwidth value updated Successfully!.');

		}

	}

	/**
	 * Remove the specified Default Bandwidth Type from storage.
	 *
	 * @param  int  $id
	 * @return default bandwidth type listing page.
	 */
	public function destroy_bandwidth($id)
	{
		$affectedRows = Default_bandwidth::where('id', '=', $id)->delete();

		if($affectedRows){

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Default Bandwidth value  Deleted Successfully ';
			$action_log->save();
			/* action log insertion */


			return redirect('default_bandwidth')->with('flash_success', ' Default Bandwidth value  Deleted Successfully!.');
		}else{
			return redirect('default_bandwidth')->with('flash_message', 'User not deleted,Try again...');
		}

	}



	/**
	 * Display the list of Group.
	 *
	 * @return Response
	 */
	public function groups()
	{
		$group_list = DB::table('groups')->get();
		return view('configure.groups', ['group_list' => $group_list]);
	}




	/**
	 * Show  the form for editing the specified/New Customer Group.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function newgroup($id=null)
	{
		if($id){
			$group = DB::table('groups')->where('id', '=', $id)->first();
			return view('configure.editgroup', ['group' => $group]);

		}else{
			return view('configure.newgroup');
		}
	}

	/**
	 * Store a newly created Customer Group in storage.
	 *
	 * @return Response
	 */
	public function storegroup(Request $request)
	{
		$v = Validator::make($request->all(), [
			'name' => 'required',
			'description' => 'required',
		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$groups = new Groups();
			$groups->name = $request->input('name');
			$groups->description = $request->input('description');
			$groups->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' New Client Group Inserted Successfully ';
			$action_log->save();
			/* action log insertion */

			return redirect('groups')->with('flash_success', ' New Client Group Inserted Successfully.');

		}

	}


	/**
	 * Store Updated records of Customer Group in storage.
	 *
	 * @param  array  $request
	 * @return Customer Group list view
	 */
	public function update_group(Request $request)
	{
		$v = Validator::make($request->all(), [
			'name' => 'required',
			'description' => 'required',
		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$groups = Groups::find($request->input('id'));
			$groups->name = $request->input('name');
			$groups->description = $request->input('description');
			$groups->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Registered Client Group Updated Successfully ';
			$action_log->save();
			/* action log insertion */


			return redirect('groups')->with('flash_success', ' Client Group Updated Successfully.');

		}

	}

	/**
	 * Remove the specified Customer Group from storage.
	 *
	 * @param  int  $id
	 * @return Customer Group listing page.
	 */
	public function destroy_group($id)
	{
		$affectedRows = Groups::where('id', '=', $id)->delete();
		if($affectedRows){

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Registered Client Group deleted Successfully ';
			$action_log->save();
			/* action log insertion */


			return redirect('groups')->with('flash_success', ' Registered Client Group deleted Successfully.');
		}else{
			return redirect('groups')->with('flash_message', 'Group not Deleted,Try again later ..');
		}

	}


	/**
	 * Display the list user/admin Rights.
	 *
	 * @return Response
	 */
	public function rights()
	{
		if(Auth::user()->access == 2){
			$user_list = DB::table('admins')->select('id', 'name')->get();
		}elseif(Auth::user()->access == 1){
			$user_list = DB::table('admins')->select('id', 'name')->where('groupid', '=', Auth::user()->groupid )->get();
		}else{
			$user_list = DB::table('admins')->select('id', 'name')->where('id', '=', Auth::user()->id)->get();
		}

		if($user_list){
			$admin_array[0] ="[Select a User]";
			foreach($user_list  as $user){
				$admin_array[$user->id]= $user->name;

			}
		}else{
			$admin_array[0] ="[No Admin for this User Level]";
		}
		return view('configure.rights', ['user_list' => $admin_array]);
	}


	/**
	 * Ajax Return Function for list USER/Admin rights.
	 *
	 * @return Response
	 */
	public function load_rights()
	{
		$admins = DB::table('admins')->where('id', '=', $_POST['admin_id'])->first();
		if($admins->access == 1){

			$menulist = DB::table('menus')->where('access', '!=', 2)->where('display', '=', 1)->get();
		}elseif($admins->access == 2){

			$menulist = DB::table('menus')->where('display', '=', 1)->get();
		}else{

			$menulist = DB::table('menus')->where('access', '=', 0)->where('display', '=', 1)->get();
		}

		$rights_array = array();
		$rights_list = DB::table('admin_modules')->select('id', 'moduleid', 'access')->where('userid', '=', $_POST['admin_id'])->get();
		foreach($rights_list  as $rights ){
			$rights_array[]= $rights->moduleid;

		}



		return view('configure.load_rights', ['rights_list' => $rights_array ,'menu_list' => $menulist]);

	}


	/**
	 * Store Updated records of User Rights in storage.
	 *
	 * @param  array  $request
	 * @return User rights list view
	 */
	public function update_rights(Request $request)
	{

		$v = Validator::make($request->all(), [
			'rights_user' => 'required',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{
			Admin_modules::where('userid', '=', $request->input('rights_user'))->delete();
			if($request->input('module')){
				foreach($request->input('module') as $key => $value ){
					$modules = new Admin_modules();
					$modules->userid = $request->input('rights_user');
					$modules->moduleid = $value;
					$modules->access = 1;
					$modules->save();
				}

			}

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' User Rights Updated for user('.$request->input('rights_user').') ';
			$action_log->save();
			/* action log insertion */



			return redirect('rights')->with('flash_success', ' User Rights Updated Successfully.');

		}
	}

	/**
	 * Display the list of Static Bandwidth type.
	 *
	 * @return Response
	 */
	public function statictype()
	{
		$bandwidth_list = DB::table('bod_bandwidth')->where('type', '=', 0)->get();
		return view('configure.statictype', ['bandwidth_list' => $bandwidth_list]);
	}



	/**
	 * Show  the form for editing the specified/New Static Bandwidth Type.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function newstatic($id = null)
	{
		if($id){
			$bandwidth = DB::table('bod_bandwidth')->where('id', '=', $id)->first();
			return view('configure.edit_static', ['bandwidth' => $bandwidth]);

		}else{
			return view('configure.new_static');
		}
	}

	/**
	 * Store a newly created Static Bandwidth Type in storage.
	 *
	 * @return Response
	 */
	public function storestatic(Request $request)
	{

		$v = Validator::make($request->all(), [
			'name' => 'required',
			'price' => 'required|Numeric',
			'value' => 'required|Numeric',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$bandwidth = new Bod_bandwidth();
			$bandwidth->bod_name = $request->input('name');
			$bandwidth->price = $request->input('price');
			$bandwidth->value = $request->input('value');
			$bandwidth->type = 0;
			$bandwidth->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' New Static Bandwidth Type added  ';
			$action_log->save();
			/* action log insertion */


			return redirect('statictype')->with('flash_success', ' New Static Bandwidth Type added Successfully.');

		}
	}


	/**
	 * Store Updated records of Static Bandwidth type in storage.
	 *
	 * @param  array  $request
	 * @return Static Bandwidth type list view
	 */
	public function update_static(Request $request)
	{
		$v = Validator::make($request->all(), [
			'name' => 'required',
			'price' => 'required|Numeric',
			'value' => 'required|Numeric',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$bandwidth = Bod_bandwidth::find($request->input('id'));
			$bandwidth->bod_name = $request->input('name');
			$bandwidth->price = $request->input('price');
			$bandwidth->value = $request->input('value');
			$bandwidth->type = 0;
			$bandwidth->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Static Bandwidth Type Updated  by '.Auth::user()->name;
			$action_log->save();
			/* action log insertion */


			return redirect('statictype')->with('flash_success', ' Static Bandwidth Type Updated Successfully.');

		}
	}


	/**
	 * Remove the specified Static Bandwidth Type from storage.
	 *
	 * @param  int  $id
	 * @return Static type listing page.
	 */
	public function destroy_static($id)
	{
		$affectedRows = Bod_bandwidth::where('id', '=', $id)->delete();
		if($affectedRows){

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Static Bandwidth Type Deleted  ';
			$action_log->save();
			/* action log insertion */

			return redirect('statictype')->with('flash_success', ' Static Bandwidth Type deleted Successfully.');
		}else{
			return redirect('statictype')->with('flash_message', 'Bandwidth type not deleted,Try again later...');
		}

	}



	/**
	 * Display the list of Dynamic Bandwidth Type.
	 *
	 * @return Response
	 */
	public function dynamictype()
	{
		$bandwidth_list = DB::table('bod_bandwidth')->where('type', '=', 1 )->get();
		return view('configure.dynamictype', ['bandwidth_list' => $bandwidth_list]);
	}



	/**
	 * Show  the form for editing the specified/New Dyanamic Bandwidth Type.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function  newdynamic($id = null)
	{
		if($id){
			$bandwidth = DB::table('bod_bandwidth')->where('id', '=', $id)->first();
			return view('configure.edit_dynamic', ['bandwidth' => $bandwidth]);

		}else{
			return view('configure.new_dynamic');
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storedynamic(Request $request)
	{

		$v = Validator::make($request->all(), [
			'name' => 'required',
			'price' => 'required|Numeric',
			'value' => 'required|Numeric',
			'max_value' => 'required|Numeric',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$bandwidth = new Bod_bandwidth();
			$bandwidth->bod_name = $request->input('name');
			$bandwidth->price = $request->input('price');
			$bandwidth->value = $request->input('value');
			$bandwidth->max_value = $request->input('max_value');
			$bandwidth->type = 1;
			$bandwidth->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' New Dynamic Bandwidth Type added  ';
			$action_log->save();
			/* action log insertion */


			return redirect('dynamictype')->with('flash_success', ' New Dynamic Bandwidth Type added Successfully.');

		}

	}

	/**
	 * Store Updated records of Dynamic Bandwidth type in storage.
	 *
	 * @param  array  $request
	 * @return Dynamic Bandwidth type list view
	 */
	public function update_dynamic(Request $request)
	{
		$v = Validator::make($request->all(), [
			'name' => 'required',
			'price' => 'required|Numeric',
			'value' => 'required|Numeric',
			'max_value' => 'required|Numeric',

		]);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			$bandwidth = Bod_bandwidth::find($request->input('id'));
			$bandwidth->bod_name = $request->input('name');
			$bandwidth->price = $request->input('price');
			$bandwidth->value = $request->input('value');
			$bandwidth->max_value = $request->input('max_value');
			$bandwidth->type = 1;
			$bandwidth->save();

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Dynamic Bandwidth Type updated ';
			$action_log->save();
			/* action log insertion */


			return redirect('dynamictype')->with('flash_success', ' Dynamic Bandwidth Type updated  Successfully.');

		}
	}


	/**
	 * Remove the specified Dynamic Bandwidth Type from storage.
	 *
	 * @param  int  $id
	 * @return Dynamic type listing page.
	 */
	public function destroy_dynamic($id)
	{
		$affectedRows = Bod_bandwidth::where('id', '=', $id)->delete();
		if($affectedRows){

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Dynamic Bandwidth Type Deleted  ';
			$action_log->save();
			/* action log insertion */

			return redirect('dynamictype')->with('flash_success', ' Dynamic Bandwidth Type Deleted  Successfully.');
		}else{
			return redirect('dynamictype')->with('flash_message', 'Bandwidth type not deleted,Try again later...');
		}

	}



	/**
	 * Display the list of interfaces.
	 *
	 * @return Response
	 */
	public function interfaces()
	{
		$interfaces_list = DB::table('interfaces')->get();
		return view('configure.interfaces', ['interfaces_list' => $interfaces_list, 'test_return' => 'sdsdsdssd']);
	}


	/**
	 * Store Updated records in storage.
	 *
	 * @param  array  $request
	 * @return interface list view
	 */
	public function update_interface(Request $request)
	{
		$interface_array = array();

		foreach($request->input('interface') as $key => $value )
		{
			$interface_array['interface['.$key.']'] = 'required';
		}
		$v = Validator::make($request->all(), [
			'interface' => 'required',

		]);

//		$v = Validator::make($request->all(), $interface_array);

		if ($v->fails())
		{
			return redirect()->back()->withErrors($v->errors());
		}else{

			foreach($request->input('interface') as $key => $value ){
				$interface = Interfaces::find($key);
				$interface->description = $value;
				$interface->save();
			}

			/* action log insertion */
			$action_log = new Action_log();
			$action_log->userid = Auth::user()->id;
			$action_log->username = Auth::user()->name;
			$action_log->action = ' Line interfaces Updated Successfully  ';
			$action_log->save();
			/* action log insertion */

			return redirect('interfaces')->with('flash_success', ' Line interfaces Updated Successfully.');

		}

	}




	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
